var widify = (function(w, d) {
    'use strict';
    function widify() {
        var $links = d.querySelectorAll('[data-wide]'),
            target = function($elt) {
                if($elt.tagName == 'BODY') {
                    return false;
                }
                if ($elt.attributes.length && 'data-wide-target' in $elt.attributes) {
                    return $elt;
                } else {
                    return target($elt.parentNode);
                }
            },
            targetsOverHandler = function(e) {
                e.target.style.cursor = 'pointer';
            },
            targetsClickHandler = function(e) {
                for (var elt = e.target; elt.nodeName; elt = elt.parentElement) {
                    if('data-wide-ignore' in elt.attributes || elt.tagName == 'BODY'){
                        break;
                    }
                    if('data-wide-target' in elt.attributes){
                        if(e.target.nodeName != 'A' && e.which != 3) {
                            definedTargetLink(this.$link, e);
                        }
                        break;
                    }
                }
            },
            definedTargetLink = function(element,eventObj){
              var targetAttr = ('target' in element.attributes) ? element.target : '_self';
                if (document.getElementsByTagName('base').length && document.getElementsByTagName('base')[0].getAttribute('target').length) {
                    targetAttr = document.getElementsByTagName('base')[0].getAttribute('target');
                }
                if(eventObj.which == 2 || eventObj.metaKey || eventObj.ctrlKey) {
                    targetAttr = '_blank';
                }
              w.open(element.href,targetAttr);
            };
        for (var i = 0, len = $links.length; i < len; i++) {
            var $link = $links[i],
                $target = target($link);

            if(!$target) {
              if (console) console.warn('You need a data-wide-target attribute on a parent for that link =>', $link);
            }

            if(!$target.$link && $target){
                $target.$link = $link; // store related link on target node.
                $target.onclick = targetsClickHandler;
                $target.onmouseover = targetsOverHandler;
            }
        }
    }

    if (window.addEventListener) window.addEventListener("load", widify, false);
    else if (window.attachEvent) window.attachEvent("onload", widify);
    else window.onload = widify;

    return widify;

})(window, document);
