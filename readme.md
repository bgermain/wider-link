# Wider link

This script is useful to make a link bigger than it effectively is. This is particularly useful for lists of items containing multiple types of content : images, text and link that you want the whole item to be clickable.

## Download

[Latest release](https://bitbucket.org/bgermain/wider-link)

## Usage

First link the script to your page.
```html
<script type="text/javascript" src="wider-link.js"></script>
```

Now you need to declare all the links you want to widify and "how far" they will widden by using data-wide and data-wide-target attributes.

```html
<div data-wide-target>
	<img src="image.jpg" alt="my image">
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, alias.</p>
	<a href="article.html" data-wide>Read</a>
	<a href="other-things.html" data-wide-ignore>This link is ignored</a>
</div>
```

That's all folk's

## One more thing

If you need to widden any link later after your page have been loaded, you can run the widify method

```javascript
widify()
```

## License

Copyright (C) 2014 bgermain

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
